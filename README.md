# Show-PSNotification #

Powershell notifications.
This function creates small HTA file and launches it.

### Parameters ###
* Size - Notification site. Small - 150x400, Big - 260x400
* Title - Notification title. Accepts text or HTML code 
* Message - Notification body. Accepts text or HTML code
* Timeout - Time to wait until closing HTA app
* Icon - Notification icon. Valid options: Alert, Rocket, Maintenance, Schedule
* Sound - Play windows asterisk sound
* CatchCloseButton - Writes output 'True' if button close was pressed

### Examples ###

* Load function

```
#!powershell
iex ((new-object net.webclient).DownloadString('https://bitbucket.org/aartemjevas/show-psnotification/raw/master/Show-PSNotification.ps1'))

```

* Big alert notification

```
#!powershell

Show-PSNotification -Size Big -Title "Alert" -Message "This is big alert body" -Wait -Icon Alert
```

![biglaert.png](https://bitbucket.org/repo/BRopde/images/1640632038-biglaert.png)

* Small schedule notification

```
#!powershell

Show-PSNotification -Size Small -Title "Update" -Message "Pending application updates <br/> 1. Firefox <br/> 2. Chrome" -Timeout 15 -Icon Schedule
```

![smallschedule.png](https://bitbucket.org/repo/BRopde/images/1358025834-smallschedule.png)


* Reboot computer if close button was pressed


```
#!powershell
$buttonPressed = Show-PSNotification -Title 'PC reboot' -Message 'PC will be rebooted when you click button close' -Timeout 120 -CatchCloseButton
if ($buttonPressed)
{
    Restart-Computer -Force
}

```